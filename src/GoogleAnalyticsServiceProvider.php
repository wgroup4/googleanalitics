<?php

namespace Wgroup\GoogleAnalytics;

use Illuminate\Support\ServiceProvider;

class GoogleAnalyticsServiceProvider extends ServiceProvider
{

    protected $defer = true;
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if (!class_exists('GoogleAnalytics')) {
            class_alias('Wgroup\GoogleAnalytics\Facades\GoogleAnalytics', 'GA');
        }

        $this->publishes([
            __DIR__.'/../config/ga.php' => config_path('ga.php'),
        ]);
    }
}
