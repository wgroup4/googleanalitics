<?php

namespace Wgroup\GoogleAnalytics\Facades;

use Illuminate\Support\Facades\Facade;

class GoogleAnalytics extends Facade
{

    /**
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'GA';
    }
}
