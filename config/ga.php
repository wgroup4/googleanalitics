<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Define which configuration should be used
    |--------------------------------------------------------------------------
    */

    'properties' => [
        'ga_tid' => 'G-RNZ0R2P68B',
        'ga_tid_mirror1' => 'G-FH5CJEGCJ3',
    ]

];
